import logging
import requests


class TelegramHandler(logging.Handler):
    def __init__(
            self,
            bot_token,
            chat_id,
            parse_mode=None,
            disable_web_page_preview=None,
            disable_notification=None,
    ):
        """
        are the same as bot api https://core.telegram.org/bots/api#sendmessage
        :param bot_token: token your bot
        :param chat_id: chat id where to send messages
        :param parse_mode: 'HTML' or 'MARKDOWN', easy way 'HTML'
        :param disable_web_page_preview: Disables link previews for links in this message
        :param disable_notification: Sends the message silently. Users will receive a notification with no sound.
        """
        logging.Handler.__init__(self)
        self.url = 'https://api.telegram.org/bot' + bot_token + '/sendMessage'
        self.chat_id = chat_id
        self.parse_mode = parse_mode
        self.disable_web_page_preview = disable_web_page_preview
        self.disable_notification = disable_notification

    def emit(self, record):
        if record.module != 'connectionpool':
            """
                Protection against recursion with logging of sending messages in telegram
            """
            data = {
                'chat_id': self.chat_id,
                'text': self.format(record)
            }

            if self.parse_mode:
                data['parse_mode'] = self.parse_mode

            if self.disable_web_page_preview:
                data['disable_web_page_preview'] = self.disable_web_page_preview

            if self.disable_notification:
                data['disable_notification'] = self.disable_notification

            requests.post(self.url, data=data)
