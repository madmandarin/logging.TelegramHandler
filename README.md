# logging.TelegramHandler

Logging Handler for sending logs in telegram

## Dependencies ##

* Python3
* Logging
* Requests 

## How to use ##

Import and connect the TelegramHandler in the same way as standard

```
th = TelegramHandler(
    bot_token= token your bot,
    chat_id= chat id where to send messages
)
formatter = logging.Formatter(fmt='%(message)')
th.setFormatter(formatter)
th.setLevel(logging.ERROR)

lg.addHandler(th)
```